const minute = 60 * 1000;

class Blind {
    constructor(blindConfig){
        //BASIC CONFIG
        this.name = blindConfig.name;
        this.serialPort = blindConfig.serialPort;
        
        this.riseCode = blindConfig.code.up;
        this.lowerCode = blindConfig.code.down;

        this.maxMovingTime = blindConfig.movingTime;

        this.blindId = this.riseCode + this.lowerCode;
        this.status = "still";
        this.movingTimeout = null;
    }

    getStatus(extendet){
        let returnObject = {
            "name" : this.name,
            "id" : this.blindId
        }

        if(extendet){
            returnObject.status = this.status;
        }
        
        return returnObject;
    }

    rise(){
        this.status = "rise";
        clearTimeout(this.movingTimeout);

        this.serialPort.write(this.riseCode + "1n");

        this.movingTimeout = setTimeout(() => {
            this.serialPort.write(this.riseCode + "0n");
        }, this.maxMovingTime * minute);
    }

    lower(){
        this.status = "lower";
        clearTimeout(this.movingTimeout);
        
        this.serialPort.write(this.lowerCode + "1n");

        this.movingTimeout = setTimeout(() => {
            this.serialPort.write(this.lowerCode + "0n");
        }, this.maxMovingTime * minute)
    }

    stop(perDefault){
        return new Promise(resolve => {
            if(!perDefault || this.status != "still"){
                if(this.status = "rise"){
                    this.serialPort.write(this.riseCode + "0n");
                }
                else if(this.status = "lower"){
                    this.serialPort.write(this.lowerCode + "0n");
                }
            }
            
            clearTimeout(this.movingTimeout);
            this.status = "still";
            
            resolve();
        });
    }
}

module.exports = Blind;
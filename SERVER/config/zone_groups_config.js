const zoneGroupsConfig = {
    "groups" : [
        {
            "name" : "all",
            "zones" : [
                "sofa",
                "bed"
                // "lounge",
                // "diningroom",
                // "diningdesk",
                // "livingroom"
            ]
        }
    ]
}

module.exports = zoneGroupsConfig;
rgbApp.controller("storageDialogCtrl", ["$scope", "rgbwColors", "colorStorage", "$rootScope", function($scope, rgbwColors, colorStorage, $rootScope) {
    $scope.showStorageDialog = false;

    $scope.$on("saveColor", () => {
        $scope.showStorageDialog = true;

        colorStorage.getStoredColors().then((response) => {
            let data = response.data;
            $scope.storedColors = data;
        });
    });

    $scope.setColor = function(elColor, index){
        let color = rgbwColors.getColor();

        colorStorage.saveColor(color, index).then(() => {
            $scope.showStorageDialog = false;
    
            $rootScope.$broadcast("storageUpdate");
        });
    };

    $scope.closeStorageDialog = function(){
        $scope.showStorageDialog = false;
    }
}]);
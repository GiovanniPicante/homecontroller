String inputString = "";
bool resetAfterManualOverwrite = true;
bool analogSignalFeedbackSend = false;

bool manualAllUPActive = false;
bool manualAllDOWNActive = false;

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 8 bytes for the inputString:
  inputString.reserve(8);

  pinMode(A5, OUTPUT);
  digitalWrite(A5, HIGH);

  for (int i = 2; i < 18; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, HIGH);
  }
}

void setAllPinsHigh () {
  for (int i = 2; i < 18; i++) {
    digitalWrite(i, HIGH);
  }

  delay(100);
}


void setAllUp(bool value) {
  for (int i = 2; i < 10; i++) {
    digitalWrite(i, value);
  }
}

void setAllDown(bool value) {
  for (int i = 10; i < 18; i++) {
    digitalWrite(i, value);
  }
}

void handleAllUp(String pinState) {
  if (pinState == "1") {
    setAllUp(false);
  }
  else if (pinState != "0") {
    setAllUp(true);
  }
}

void handleAllDown(String pinState) {
  if (pinState == "1") {
    setAllDown(false);
  }
  else if (pinState != "0") {
    setAllDown(true);
  }
}

void handleInput (String pinNumber, String pinState) {
  //For safty: set all pins high
  setAllPinsHigh();

  Serial.println(pinNumber + pinState);

  if (pinNumber == "AU") {
    handleAllUp(pinState);
    return;
  }
  else if (pinNumber == "AD") {
    handleAllDown(pinState);
    return;
  }

  int intPinNumber = pinNumber.toInt();

  if (pinState == "1") {
    digitalWrite(intPinNumber, LOW);
  }
  else if (pinState != "0") {
    digitalWrite(intPinNumber, HIGH);
  }
}

void analogSignalFeedback() {
  if (!analogSignalFeedbackSend) {
    Serial.println("OVERWRITE");
    analogSignalFeedbackSend = true;
  }
}

void loop() {
  int testA6 = analogRead(A6);
  int testA7 = analogRead(A7);

  if (testA7 > 800) {
    if(manualAllDOWNActive == false){
      //For safty: set all pins high
      setAllPinsHigh();
      handleAllDown("1");
      manualAllDOWNActive = true;
      manualAllUPActive = false;
    }

    analogSignalFeedback();
    resetAfterManualOverwrite = false;
  }
  else if (testA6 > 800) {
    if(manualAllUPActive == false){
      //For safty: set all pins high
      setAllPinsHigh();
      handleAllUp("1");
      manualAllUPActive = true;
      manualAllDOWNActive = false;
    }

    analogSignalFeedback();
    resetAfterManualOverwrite = false;
  }
  else if (testA6 < 800 && testA7 < 800 && !resetAfterManualOverwrite) {
    setAllPinsHigh();
    resetAfterManualOverwrite = true;
    analogSignalFeedbackSend = false;
    manualAllUPActive = false;
    manualAllDOWNActive = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    inputString += inChar;

    if (inputString.endsWith("n")) {
      String pinNumber = inputString.substring(0);
      pinNumber = pinNumber.substring(0, 2);

      String pinStatus = inputString.substring(2);
      pinStatus = pinStatus.substring(0, 1);

      handleInput(pinNumber, pinStatus);

      //Clear inputString
      inputString = "";
    }
  }
}

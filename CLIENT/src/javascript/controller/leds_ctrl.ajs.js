sessionStorage.clear();

rgbApp.controller("ledsCtrl", ["$scope", "$routeParams", "zoneStatus", "$timeout", "rgbwColors", "colorStorage", function($scope, $routeParams, zoneStatus, $timeout, rgbwColors, colorStorage) {
    $scope.allZones = [];
    $scope.activeZone = $routeParams.zone;
    $scope.activeZoneType = "rgb";
    $scope.colorObject = '{"r":0, "g":0, "b":0, "w":0}';
    $scope.onOrOff = false;

    function initAllZones(zonesConfig){
        $scope.allZones = zonesConfig.zones;
    }

    function init(){
        let zonesConfig = sessionStorage.getItem("zonesConfig");
        
        if(zonesConfig == null){
            zoneStatus.getZonesConfig().then(response => {
                initAllZones(response.data);
                sessionStorage.setItem("zonesConfig", JSON.stringify(response.data));
            });
        }
        else{
            initAllZones(JSON.parse(zonesConfig));
        }
    }
    init();

    zoneStatus.getZoneStatus($scope.activeZone).then(response => {
        let data = response.data;
        $scope.onOrOff = data.active;
        $scope.colorObject = JSON.stringify(data.color);
        $scope.slider = data.color.w;
        $scope.activeZoneType = data.type;
        rgbwColors.init(data.color);

        console.log(data);
        
        $timeout(() => {
            $scope.$broadcast("initColorWheel");
        });
    });

    function colorStorageUpdate(){
        colorStorage.getStoredColors().then((response) => {
            let data = response.data;
            $scope.storedColors = data;
        });
    }

    $scope.toggleOnOff = () => {
        rgbwColors.toggleOnOff($scope.activeZone);
    };

    colorStorageUpdate();
    $scope.$on("storageUpdate", function(){
        colorStorageUpdate();
    });
    
    $scope.setColor = function(colorObject){
        $scope.colorObject = colorObject;
        $scope.$broadcast("updateColor");
    }
}]);
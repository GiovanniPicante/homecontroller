const request = require("request");

const mode = process.argv[2] == "debug" ? "debug" : "live";

let pigpio;
let Gpio;

if(mode == "live"){
    pigpio = require("pigpio");
    Gpio = pigpio.Gpio;
}
else{
    Gpio = function(pin, config){
        this.pwmWrite = function(){};
    };
}

class Zone {
    constructor(zoneConfig){
        //BASIC CONFIG
        this.name = zoneConfig.name;
        this.type = zoneConfig.type;
        this.address = zoneConfig.address != undefined ? zoneConfig.address : null;

        this.colorRed = 128;
        this.colorGreen = 128;
        this.colorBlue = 128;
        this.colorWhite = this.type == "rgbw" ? 128 : 0;

        this.pwmRed = this.colorRed;
        this.pwmGreen = this.colorGreen;
        this.pwmBlue = this.colorBlue;
        this.pwmWhite = this.colorWhite;

        this.active = true;

        if(this.type == "rgb" || this.type == "rgbw"){
            //GPIO CONFIG
            this.pinRed = zoneConfig.pinOut.red;
            this.pinGreen = zoneConfig.pinOut.green;
            this.pinBlue = zoneConfig.pinOut.blue;
            this.pinWhite = this.type == "rgbw" ? zoneConfig.pinOut.white : null;
        
            this.gpioRed = new Gpio(this.pinRed, { mode : Gpio.OUTPUT });
            this.gpioGreen = new Gpio(this.pinGreen, { mode : Gpio.OUTPUT });
            this.gpioBlue = new Gpio(this.pinBlue, { mode : Gpio.OUTPUT });
            this.gpioWhite = this.type == "rgbw" ? new Gpio(this.pinWhite, { mode : Gpio.OUTPUT }) : 0;

            //PWM INIT
            this.gpioRed.pwmWrite(this.pwmRed);
            this.gpioGreen.pwmWrite(this.pwmGreen);
            this.gpioBlue.pwmWrite(this.pwmBlue);
            this.type == "rgbw" ? this.gpioWhite.pwmWrite(this.pwmWhite) : 0;
        }
    }

    getStatus(){
        let colorInfo = {
            "r" : this.colorRed,
            "g" : this.colorGreen,
            "b" : this.colorBlue
        }

        if(this.type == "rgbw"){
            colorInfo.w = this.colorWhite;
        }

        return {
            "name" : this.name,
            "type" : this.type,
            "color" : colorInfo,
            "active" : this.active
        }
    }

    setColors(colors){
        this.colorRed = colors.red;
        this.colorGreen = colors.green;
        this.colorBlue = colors.blue;
        this.colorWhite = this.type == "rgbw" ? colors.white : 0;

        this.setColor();
    }

    setColor(pwmRed = this.colorRed, pwmGreen = this.colorGreen, pwmBlue = this.colorBlue, pwmWhite = this.colorWhite){
        if(this.type == "rgb" || this.type == "rgbw"){
            this.pwmRed = pwmRed;
            this.pwmGreen = pwmGreen;
            this.pwmBlue = pwmBlue;
            this.pwmWhite = pwmWhite;

            this.gpioRed.pwmWrite(pwmRed);
            this.gpioGreen.pwmWrite(pwmGreen);
            this.gpioBlue.pwmWrite(pwmBlue);
            this.type == "rgbw" ? this.gpioWhite.pwmWrite(pwmWhite) : 0;
        }
        else{
            let url = `${this.address}/setcolor?red=${this.convert8BitTo10Bit(pwmRed)}&green=${this.convert8BitTo10Bit(pwmGreen)}&blue=${this.convert8BitTo10Bit(pwmBlue)}&white=${this.convert8BitTo10Bit(pwmWhite)}`;
            
            request(url, function(err){
                if(err){
                    console.log(err);
                }
            });
        }
    }

    toggleOnOff(){
        if(this.active){
            this.setColor(0, 0, 0, 0);
            this.active = false;

            return false;
        }
        else{
            this.setColor(this.colorRed, this.colorGreen, this.colorBlue, this.colorWhite);
            this.active = true;

            return true;
        }
    }

    convert8BitTo10Bit(value8Bit){
        var percentage = Math.round((value8Bit / 255) * 100);
        var value10Bit = (1023 / 100) * percentage;

        return parseInt(value10Bit);
    }
}

module.exports = Zone;
rgbApp.factory("blindsControll", ["$http", function($http) {
    function riseBlind(blindId){
        let url = "/blindRise?blindId=" + blindId;
        $http({
            "method" : "GET",
            "url" : url
        });
    }

    function lowerBlind(blindId){
        let url = "/blindLower?blindId=" + blindId;
        $http({
            "method" : "GET",
            "url" : url
        });
    }

    function stopBlind(blindId){
        let url = "/blindStop?blindId=" + blindId;
        $http({
            "method" : "GET",
            "url" : url
        });
    }

    return {
        rise : riseBlind,
        lower : lowerBlind,
        stop : stopBlind
    };
}]);
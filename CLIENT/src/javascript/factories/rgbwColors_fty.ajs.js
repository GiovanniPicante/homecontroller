rgbApp.factory("rgbwColors", ["$http", function($http) {
    let red = 255;
    let green = 255;
    let blue = 255;
    let white = 0;

    function init(colorJSON){
        red = colorJSON.r;
        green = colorJSON.g;
        blue = colorJSON.b;
        white = colorJSON.w;
    }

    function setColor(zone){
        let colorJSON = {
            "red" : red,
            "green" : green,
            "blue" : blue,
            "white" : white
        }

        $http({
            "method" : "POST",
            "url" : "/setColor",
            "data" : {
                "zone" : zone,
                "colors" : colorJSON
            }
        });
    }

    function toggleOnOff(zone){
        $http({
            "method" : "POST",
            "url" : "/toggleOnOff",
            "data" : {
                "zone" : zone
            }
        });
    }

    function getColor(){
        return {
            "r" : red,
            "g" : green,
            "b" : blue,
            "w" : white
        };
    };

    function update(zone, type, colorJSON){
        if(type == "rgb"){
            red = colorJSON.r;
            green = colorJSON.g;
            blue = colorJSON.b;
        }
        else{
            white = colorJSON.w;
        }

        setColor(zone);
    }

    return {
        init: init,
        set : setColor,
        toggleOnOff : toggleOnOff,
        getColor : getColor,
        update : update
    };
}]);
rgbApp.factory("colorStorage", ["$http", function($http) {
    function getStoredColors(){
        return $http({
            "method" : "GET",
            "url" : "/storedColors"
        });
    }

    function saveColor(colorObject, index){
        return $http({
            "method" : "POST",
            "url" : "/saveColor",
            "data" : { "color" : colorObject, "index" : index }
        });
    }

    return {
        getStoredColors : getStoredColors,
        saveColor : saveColor
    };
}]);
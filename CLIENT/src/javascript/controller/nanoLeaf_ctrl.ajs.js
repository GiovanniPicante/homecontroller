rgbApp.controller("nanoLeafCtrl", ["$scope", "$http", function($scope, $http) {
    $scope.nanoLeafConfig = [];
    $scope.onOrOff = false;
    $scope.brightness = 0;

    $http({
        "method": "GET",
        "url": "/nanoLeaf/config"
    }).then(response => {
        let nanoLeafConfig = response.data; 
        $scope.nanoLeafConfig = nanoLeafConfig;

        // console.log($scope.nanoLeafConfig);

        if(nanoLeafConfig[0].value == 1){
            $scope.onOrOff = true;
        }
        else{
            false;
        }

        $scope.brightness = nanoLeafConfig[1].value == 1 ? 0 : nanoLeafConfig[1].value;
    })

    $scope.toggleOnOff = () => {
        $http({
            "method": "GET",
            "url": "/nanoLeaf/toggelOnOff"
        }).then(response => {
            // console.log(response);
        })
    };

    $scope.brightnessChange = function(){
        let value = $scope.brightness;
        
        $http({
            "method": "GET",
            "url": "/nanoLeaf/brightness?value=" + value
        }).then(response => {
            // console.log(response);
        })
    }
}]);
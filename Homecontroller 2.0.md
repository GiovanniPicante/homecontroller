# Homecontroller Version 2

### Neue Feature
1) Support für HomeNodeLED
2) Support für Smart Nanoleaf Replica
3) Dark Mode
4) Gespeicherte Farben können benannt werden
5) Beleuchtungs Szenerien

### Bugfixes
1) Verbesserter Master An/Aus Button

### Änderungen
1) Neue JavaScript-bassierte Config für LEDs
   - Typ: String (rgb, rgbw, rgb_wifi, rgbw_wifi, nano_leaf_wifi)
   - Name: String
   - Icon: String
   - Szenario-Gruppe: String
   - Hostadresse: String
   - API-Info: JSON
   - PIN-Out: JSON
2) Neues Logo / Icon
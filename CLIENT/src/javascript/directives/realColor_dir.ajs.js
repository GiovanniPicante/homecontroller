rgbApp.directive("realColor", [function(){
    return{
        restrict : "A",
        scope : true,
        controller : ["$scope", "$element", function($scope, $element){
            let rot = $scope.color.r;
            let gruen = $scope.color.g;
            let blau = $scope.color.b;

            let vRot, vGruen, vBlau;
            let nRot, nGruen, nBlau;

            let max = Math.max(rot, gruen, blau);

            if (max == rot) {
                vRot = 1;
                vGruen = gruen / rot;
                vBlau = blau / rot;

                nRot = 255;
                nGruen = Math.round(255 * vGruen);
                nBlau = Math.round(255 * vBlau);
            }
            else if (max == gruen) {
                vRot = rot / gruen;
                vGruen = 1;
                vBlau = blau / gruen;

                nRot = Math.round(255 * vRot);
                nGruen = 255;
                nBlau = Math.round(255 * vBlau);
            }
            else if (max == blau) {
                vRot = rot / blau;
                vGruen = gruen / blau;
                vBlau = 1;

                nRot = Math.round(255 * vRot);
                nGruen = Math.round(255 * vGruen);
                nBlau = 255;
            }

            let rgb = "rgb(" + nRot + "," + nGruen + "," + nBlau + ")";
            $element.css("background-color", rgb);
        }]
    }
}]);
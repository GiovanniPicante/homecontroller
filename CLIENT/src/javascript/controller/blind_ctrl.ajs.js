rgbApp.controller("blindCtrl", ["$scope", "$timeout", "blindsControll", function($scope, $timeout, blindsControll) {
    $timeout(() => {
        const blind = $scope.blind;
        const element = jQuery("#" + blind.id);
        const buttonUp = element.find(".upButton");
        const buttonDown = element.find(".downButton"); 
    
        buttonUp.on("touchstart", () => {
            blindsControll.rise(blind.id);
        });

        buttonDown.on("touchstart", () => {
            blindsControll.lower(blind.id);
        });
    
        buttonUp.on("touchend", () => {
            blindsControll.stop(blind.id);
        });
    
        buttonDown.on("touchend", () => {
            blindsControll.stop(blind.id);
        });

        $scope.rise = function(){
            blindsControll.rise(blind.id);
        }
    
        $scope.lower = function(){
            blindsControll.lower(blind.id);
        }
    });
}]);
rgbApp.directive("whiteSlider", ["rgbwColors", function(rgbwColors){
    return{
        restrict : "A",
        controller : ["$scope", function($scope){
            $scope.$on("initColorWheel", () => {
                $scope.valueChange = () => {
                    rgbwColors.update($scope.activeZone, "w", { "w" : $scope.slider });
                };

                $scope.$on("updateColor", function(){
                    $scope.slider = $scope.colorObject.w;
                    $scope.valueChange();
                });
            });
        }]
    }
}]);
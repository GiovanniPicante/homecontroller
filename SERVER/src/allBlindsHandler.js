const minute = 60 * 1000;

// MODULE =======================================================================================================================
class AllBlindsHandler {
    constructor(allBlindsHandlerConfig){
        this.serialPort = allBlindsHandlerConfig.serialPort;

        this.movingTimeout = null;
        this.maxTime = 2 * minute;

        this.active = "still";
    }

    allBlindsUp(){
        this.serialPort.write("AU1n");
        this.active = "up";

        this.movingTimeout = setTimeout(() => {
            this.serialPort.write("AU0n");
            this.active = "still";
        }, this.maxTime);
    }

    allBlindsDown(){
        this.serialPort.write("AD1n");
        this.active = "down";        

        this.movingTimeout = setTimeout(() => {
            this.serialPort.write("AD0n");
            this.active = "still";
        }, this.maxTime);
    }

    allBlindsStop(byOverwrite){
        return new Promise(resolve => {
            if(!byOverwrite){
                if(this.active == "up"){
                    this.serialPort.write("AU0n");
                }
                else if(this.active == "down"){
                    this.serialPort.write("AD0n");
                }
            }
            
            clearTimeout(this.movingTimeout);

            this.active = "still";

            resolve();
        });
    }
}

module.exports = AllBlindsHandler;
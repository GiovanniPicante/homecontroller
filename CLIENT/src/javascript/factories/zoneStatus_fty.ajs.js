rgbApp.factory("zoneStatus", ["$http", function($http) {
    function getZonesConfig(){
        return $http({
            "method" : "GET",
            "url" : "/zonesConfig"
        });
    }

    function getZoneStatus(zone){
        return $http({
            "method" : "GET",
            "url" : "/status?zone=" + zone
        });
    }

    return {
        getZoneStatus : getZoneStatus,
        getZonesConfig : getZonesConfig
    };
}]);
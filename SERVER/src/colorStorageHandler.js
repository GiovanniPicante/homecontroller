const fs = require("fs-extra");

const emptyStorageItem = {
    "r" : 128,
    "g" : 128,
    "b" : 128,
    "w" : 128
};

// HELPER =======================================================================================================================
function generateStorageBaseConfig(){
    let baseConfig = [];

    for(let i = 15; i > 0; i--){
        baseConfig.push(emptyStorageItem);
    }

    return baseConfig;
}

function prpareStorageFile(storagePath){
    fs.ensureFileSync(storagePath);
    
    if(fs.readJsonSync(storagePath, { throws: false }) == null){
        let storageBaseConfig = generateStorageBaseConfig();
        fs.writeJsonSync(storagePath, storageBaseConfig);
    }
}

// MODULE =======================================================================================================================
class ColorStorageHandler {
    constructor(storageDir){
        this.storageDir = storageDir;
        this.storagePath = storageDir + "colorStore.json";

        prpareStorageFile(this.storagePath);
    }

    getStoredColors(){
        return fs.readJsonSync(this.storagePath);
    }

    storeColor(colorObject, index){
        let storageJson = this.getStoredColors();
        storageJson[index] = colorObject;

        fs.writeJsonSync(this.storagePath, storageJson);

        return this.getStoredColors();
    }
}

module.exports = ColorStorageHandler;
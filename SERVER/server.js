const mode = process.argv[2] == "debug" ? "debug" : "live";

//INCLUDES 
const express = require("express");
const bodyParser = require('body-parser');
const requestPromise = require("request-promise");
const requestHttp = require("request");

let app = express();
app.use(bodyParser.json());

let pigpio;
if(mode == "live"){
    pigpio = require("pigpio");
}

const Zone = require("./src/ZoneClass");
const ZoneGroup = require("./src/ZoneGroupClass");
const ColorStorageHandler = require("./src/colorStorageHandler");

//CONFIG
const serverConfig = require("./config/server_config.js");
const zonesConfig = require("./config/zones_config.js");
const zoneGroupsConfig = require("./config/zone_groups_config.js");

//SERVER CONFIG
const path = require('path');
app.use(express.static(serverConfig.clientPath));
app.use('/icons', express.static(__dirname  + "/config/icons"));

//ENV ===========================================================================================================================
let colorStorage = new ColorStorageHandler(serverConfig.colorStorageDir);
let zones = {};
let zoneGroups = {};

// let blinds = {};
// let allBlindsHandler = {};
// let blindsConfig = [];

//LED INIT ======================================================================================================================
let createZoneObjects = new Promise((resolve) => {
    let promiseArray = [];
    for(let i = 0; i < zonesConfig.zones.length; i++){
        promiseArray.push(new Promise(resolve => {
            let zone = zonesConfig.zones[i];

            zones[zone.name] = new Zone(zone);

            resolve();
        }));
    }

    Promise.all(promiseArray).then(() => {
        resolve();
    })
}).then(() => {
    for(let i = 0; i < zoneGroupsConfig.groups.length; i++){
        let groupConfig = zoneGroupsConfig.groups[i];

        let zoneList = [];
        for(let zoneName in zones){
            if(groupConfig.zones.indexOf(zoneName) != -1){
                zoneList.push(zones[zoneName]);
            }
        }

        zoneGroups[groupConfig.name] = new ZoneGroup({ name : groupConfig.name, zones : zoneList });
    }
});


// LED HELPER ===================================================================================================================
function zoneOrGroup(zoneName){
    if(zoneGroups[zoneName] !== undefined){
        return zoneGroups[zoneName]
    }
    else{        
        return zones[zoneName];
    }
}

//BLINDS HELPER =================================================================================================================
// function stopAllBlinds(){
//     return new Promise(resolve => { 
//         let promiseArray = [];
//         // promiseArray.push(allBlindsHandler.allBlindsStop(true));

//         for(let blindId in blinds){
//             promiseArray.push(blinds[blindId].stop(true));
//         }

//         Promise.all(promiseArray).then(() => {
//             resolve();
//         });
//     });
// }

//BLINDS INIT ===================================================================================================================
// if(serverConfig.blinds !== undefined && serverConfig.blinds !== null && mode == "live"){
//     const SerialPort = require('serialport');
//     // const AllBlindsHandler = require("./src/allBlindsHandler");

//     blindsConfig = require("./config/blinds_config.json");

//     const Blind = require("./src/BlindClass");

//     let serialPort = new SerialPort(serverConfig.blinds.serialAddress, {
//         baudRate: 9600
//     });
    
//     const Readline = SerialPort.parsers.Readline;
//     const serialFeedback = serialPort.pipe(new Readline({ delimiter: '\n' }));

//     //SERIAL FEEDBACK
//     serialFeedback.on('data', (feedback) => {
//         console.log("BLIND ACTIVITY: " + feedback);

//         if(feedback == "OVERWRITE\n"){
//             stopAllBlinds();
//         }
//     });

//     let createBlindObjects = new Promise((resolve) => {
//         let promiseArray = [];
//         for(let i = 0; i < blindsConfig.blinds.length; i++){
//             promiseArray.push(new Promise(resolve => {
//                 let blind = blindsConfig.blinds[i];
//                 blind.serialPort = serialPort;
    
//                 let newBlind = new Blind(blind);

//                 blinds[newBlind.blindId] = newBlind;
    
//                 resolve();
//             }));
//         }

//         Promise.all(promiseArray).then(() => {
//             resolve();
//         });
//     });
// }

//LED API =======================================================================================================================
app.get("/zonesConfig", (request, response) => {
    response.send(zonesConfig);
});

app.get("/status", (request, response) => {
    let zoneName = request.query.zone;
    let responseJSON = {};

    responseJSON = zoneOrGroup(zoneName).getStatus();

    response.send(responseJSON);
});

app.post("/setColor", (request, response) => {
    let dataJSON = request.body;
    zoneOrGroup(dataJSON.zone).setColors(dataJSON.colors);

    response.send("colorSet");
});

app.post("/toggleOnOff", (request, response) => {
    let dataJSON = request.body;
    let zoneName = dataJSON.zone;
    let zoneActive = zoneOrGroup(zoneName).toggleOnOff();

    let responseJSON = {
        "zone" : zoneName,
        "active" : zoneActive
    }

    response.send(responseJSON);
});

app.get("/storedColors", (request, response) => {
    let storageJson = colorStorage.getStoredColors();
    response.send(storageJson);
});

app.post("/saveColor", (request, response) => {
    let dataJSON = request.body;
    let storageJson = colorStorage.storeColor(dataJSON.color, dataJSON.index);

    response.send(storageJson);
});

//BLINDS API ====================================================================================================================
// app.get("/blindsConfig", (request, response) => {
//     let responseJSON = [];

//     new Promise(resolve => {
//         let promiseArray = [];

//         for(let blindId in blinds){
//             promiseArray.push(new Promise(resolve => {
//                 responseJSON.push(blinds[blindId].getStatus());
//                 resolve();
//             }));
//         }

//         Promise.all(promiseArray).then(() => {
//             response.send(responseJSON);
//             resolve();
//         });
//     });
// });

// app.get("/blindRise", (request, response) => {
//     let blindId = decodeURIComponent(request.query.blindId);

//     let lastBlindStatus = blinds[blindId].getStatus(true);

//     stopAllBlinds().then(() => {
//         if(lastBlindStatus.status != "rise"){
//             blinds[blindId].rise();
//         }
//         response.send("rising");
//     });
// });

// app.get("/blindLower", (request, response) => {
//     let blindId = decodeURIComponent(request.query.blindId);

//     let lastBlindStatus = blinds[blindId].getStatus(true);

//     stopAllBlinds().then(() => {
//         if(lastBlindStatus.status != "lower"){
//             blinds[blindId].lower();
//         }
//         response.send("lowering");
//     });
// });


// app.get("/blindStop", (request, response) => {
//     let blindId = decodeURIComponent(request.query.blindId);

//     blinds[blindId].stop();

//     response.send("stoping");
// });

//NANO LEAF API =================================================================================================================
app.get('/nanoLeaf/config', function(request, response) {
    requestPromise.get("http://smartnanoleafs/config.json").then(config => {
        response.send(config);
    })
});

app.get('/nanoLeaf/toggelOnOff', function(request, response) {
    requestPromise.get("http://smartnanoleafs/power?value=toggle").then(state => {
        if(state == 1){
            response.send("on");
        }
        else{
            response.send("off");
        }
    })
});

app.get('/nanoLeaf/brightness', function(request, response) {
    let value = request.query.value;
    requestHttp.get("http://smartnanoleafs/brightness?value=" + value);
    response.send("ok");
});

//BASE API ======================================================================================================================
app.get('/', function(request, response) {
    response.sendFile(path.join(serverConfig.clientPath + 'index.html'));
});

//STARTUP =======================================================================================================================
app.listen(serverConfig.port);
console.log("Server runs on port " + serverConfig.port);

//SHUTDOWN ======================================================================================================================
process.stdin.resume();

let closeHandler = function(save){
    if(save){
        if(mode == "live"){
            pigpio.terminate();
        }

        console.log("\nAdee!");
    }
    
    process.exit();
};

//do something when app is closing
process.on('exit', closeHandler.bind(null, true));

//catches ctrl+c event
process.on('SIGINT', closeHandler.bind(null, false));
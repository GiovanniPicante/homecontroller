class ZoneGroup {
    constructor(zoneConfig){
        //BASIC CONFIG
        this.name = zoneConfig.name;
        this.zones = zoneConfig.zones;

        this.colorRed = 128;
        this.colorGreen = 128;
        this.colorBlue = 128;
        this.colorWhite = 128;

        this.pwmRed = this.colorRed;
        this.pwmGreen = this.colorGreen;
        this.pwmBlue = this.colorBlue;
        this.pwmWhite = this.colorWhite;

        this.active = true;
    }

    getStatus(){
        let colorInfo = {
            "r" : this.colorRed,
            "g" : this.colorGreen,
            "b" : this.colorBlue,
            "w" : this.colorWhite
        }

        return {
            "name" : this.name,
            "color" : colorInfo,
            "active" : this.active
        }
    }

    setPWM(pwmRed = this.colorRed, pwmGreen = this.colorGreen, pwmBlue = this.colorBlue, pwmWhite = this.colorWhite){
        this.pwmRed = pwmRed;
        this.pwmGreen = pwmGreen;
        this.pwmBlue = pwmBlue;
        this.pwmWhite = pwmWhite;
    }

    setColors(colors){
        this.colorRed = colors.red;
        this.colorGreen = colors.green;
        this.colorBlue = colors.blue;
        this.colorWhite = colors.white;

        this.setPWM();

        this.setZonesColor();
    }

    setZonesColor(){
        for(let zoneName in this.zones){
            let zone = this.zones[zoneName];

            let newZoneColor = {
                red : this.colorRed,
                green : this.colorGreen,
                blue : this.colorBlue,
                white : this.colorWhite
            }

            zone.setColors(newZoneColor);
        }
    }

    setZonesPWM(){
        for(let zoneName in this.zones){
            let zone = this.zones[zoneName];

            zone.toggleOnOff();
        }
    }

    toggleOnOff(){
        if(this.active){
            this.setPWM(0, 0, 0, 0);
            this.setZonesPWM();
            this.active = false;

            return false;
        }
        else{
            this.setPWM(this.colorRed, this.colorGreen, this.colorBlue, this.colorWhite);
            this.setZonesPWM();
            this.active = true;

            return true;
        }
    }
}

module.exports = ZoneGroup;
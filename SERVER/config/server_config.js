// LIVE WALSDORF
/* serverConfig = {
     "port" : 80,
     "clientPath" : "/home/pi/homecontroler/CLIENT/",
     "colorStorageDir" : "/home/pi/homecontroler/SERVER/data/",
     "blinds" : {
         "serialAddress" : "/dev/ttyUSB0"
     }
}*/

// LIVE MINE
// serverConfig = {
//     "port" : 80,
//     "clientPath" : "/home/pi/Documents/homecontroller/CLIENT/",
//     "colorStorageDir" : "/home/pi/Documents/homecontroller/SERVER/data/"
// }

// DEBUG
const serverConfig = {
    "port" : 80,
    "clientPath" : "C:/Users/Johannes/git/homecontroller/CLIENT",
    "colorStorageDir" : "C:/Users/Johannes/git/homecontroller/SERVER/data/",
}

module.exports = serverConfig;
rgbApp = angular.module('ledSteuerung', ["ngRoute"]);
rgbApp.config(["$routeProvider", "$locationProvider", ($routeProvider, $locationProvider) => {
    $locationProvider.hashPrefix("");

    $routeProvider
    .when("/leds/:zone", {
        controller : "ledsCtrl",
        templateUrl : "/src/html/leds.html"
    })
    .when("/blinds", {
        controller : "blindsCtrl",
        templateUrl : "/src/html/blinds.html"
    })
    .otherwise({
        redirectTo: "/leds/all"
    });
}]);
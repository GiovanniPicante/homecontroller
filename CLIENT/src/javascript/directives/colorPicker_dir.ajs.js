rgbApp.directive("colorPicker", ["rgbwColors", "$timeout", function(rgbwColors, $timeout){
    return{
        restrict : "A",
        link : function(scope, element, attrs){ 
            scope.$on("initColorWheel", () => {
                let colorObject = JSON.parse(attrs.initialColor);
                let initialRGB = {
                    r : colorObject.r,
                    g : colorObject.g,
                    b : colorObject.b
                };
                
                var colorPicker = new iro.ColorPicker("#color-picker-container", {
                    wheelLightness : false,
                    color : initialRGB
                });

                let rgb = initialRGB;
                let last_rgb = initialRGB;
                $timeout(() => {
                    setInterval(() => {
                        if(JSON.stringify(rgb) != JSON.stringify(last_rgb)){
                            last_rgb = rgb;
                            rgbwColors.update(scope.activeZone, "rgb", rgb);
                        }
                    }, 100);
                });

                colorPicker.on("color:change", event => {
                    rgb = colorPicker.color.rgb;
                });

                scope.$on("updateColor", () => {
                    let newColor = {
                        r : scope.colorObject.r,
                        g : scope.colorObject.g,
                        b : scope.colorObject.b
                    };

                    colorPicker.color.rgb = newColor;
                });
            });
        }
    };
}]);
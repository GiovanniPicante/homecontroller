rgbApp.controller("navbarCtrl", ["$scope", "$rootScope", function($scope, $rootScope) {
    $scope.showStorageDialog = false;
    $scope.ledControll = false;

    if(window.location.href.indexOf("leds") != -1){
        $scope.ledControll = true;
    }

    $scope.$on("$locationChangeSuccess", function(){
        if(window.location.href.indexOf("leds") != -1){
            $scope.ledControll = true;
        }
        else{
            $scope.ledControll = false;
        }
    });

    $scope.saveColor = function(){
        $rootScope.$broadcast("saveColor");
    };
}]);
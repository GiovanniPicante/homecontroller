example = {
    name: "myRGBStrip",
    type: "rgb", // rgb, rgbw, rgb_wifi, rgbw_wifi, nano_leaf
    icon: "/icons/navLED.svg",
    // address: "myHomeNodeLED:8080", // only needed when wifi is used
    pinOut: {
        green: 14,
        blue: 26,
        red: 15
    }
}

const zones_config = {
    "zones" : [
        {
            name: "sofa",
            type: "rgb_wifi",
            icon: "/icons/sofa.svg",
            address: "http://sofaRGBController"
        },
        {
            name: "bed",
            type: "rgb",
            icon: "/icons/bed.png",
            pinOut: {
                red: 13,
                green: 5,
                blue: 26
            }
        },
        {
            name: "nanoleaf",
            type: "nano_leaf",
            icon: "/icons/nanoLeaf.png",
            address: "http://smartnanoleafs/"
        },
        // {
        //     name: "lounge",
        //     type: "rgbw",
        //     icon: "/icons/lounge.svg",
        //     pinOut: {
        //         green : 14,
        //         blue: 26,
        //         red:  15,
        //         white : 19
        //     }
        // },
        // {
        //     name: "diningroom",
        //     type: "rgbw",
        //     icon: "/icons/dining.svg",
        //     pinOut: {
        //         green: 13,
        //         blue: 6,
        //         red: 5,
        //         white: 11
        //     }
        // },
        // {
        //     name: "diningdesk",
        //     type: "rgbw",
        //     icon: "/icons/diningdesk.svg",
        //     pinOut: {
        //         green: 9,
        //         blue: 22,
        //         red: 10,
        //         white: 27
        //     }
        // },
        // {
        //     name: "livingroom",
        //     type: "rgbw",
        //     icon: "/icons/sofa.svg",
        //     pinOut: {
        //         green: 17,
        //         blue: 4,
        //         red: 2,
        //         white: 3
        //     }
        // }
    ]
}

module.exports = zones_config;
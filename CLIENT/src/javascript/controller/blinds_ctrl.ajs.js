rgbApp.controller("blindsCtrl", ["$scope", "$http", function($scope, $http) {
    $scope.allBlinds = [];

    $http({
        "method" : "GET",
        "url" : "/blindsConfig"
    }).then((response) => {
        let allBlinds = response.data;

        $scope.allBlinds = allBlinds;
    });
}]);